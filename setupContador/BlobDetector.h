#pragma once
#include <opencv2/opencv.hpp>
#include "cvblob.h"

class BlobDetector
{
private:
	cv::Mat inputROI; //imagen de entrada en la region de interes
	cv::Mat processedInput; //imagen procesada con el fondo substraido (img facil de detectar los blobs)
	int minarea; //area minima para filtrar las blobls
	int maxarea;
	int thdistance; //parametros para el seguimiento de blobs , ver cvblob.h y trackblobs.cpp para mas detalles
	int thinactive;




public:
	BlobDetector();
	~BlobDetector();
	void setparameters(cv::Mat _input, cv:: Mat _procInput, int _minarea, int _maxarea, int _thdistance
		,int _thinactive);
	void getTracks(cvb::CvTracks *tracks);

};

