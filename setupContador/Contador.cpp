
#include "Contador.h"
using namespace std;

Contador::Contador()
{
}


Contador::~Contador()
{
}
void Contador::setParametros(cv::Mat *_input, cvb::CvTracks _tracks, int _puntoA, int _puntoB, int _modo,
		int _min, int _max ) {
	inputROI = *_input;
	tracks = _tracks;
	puntoA = _puntoA;
	puntoB = _puntoB;
	modo = _modo;
	minArea = _min;
	maxArea = _max;
}

void Contador::iniciarConteo(cv::vector<cv::vector<double> > *Xposiciones, cv::vector<cv::Vec4i> *trayectorias,
	int *entrantes, int *salientes){
	cv::Scalar azul = cv::Scalar(255, 0, 0);
	cv::Scalar rojo = cv::Scalar(0, 0, 255);
	cv::Scalar verde = cv::Scalar(0, 255, 0);
	cv::Scalar amarillo = cv::Scalar(0, 255, 255);
	// An�lisis de los tracks para ver si entran o salen

	//Configuraci�n de los puntos de entrada 

	//Punto A
	cv::Point P1a = cv::Point(puntoA, 0);
	cv::Point P1b = cv::Point(puntoA, inputROI.rows);
	cv::line(inputROI, P1a, P1b, rojo, 1, 0);
	cv::putText(inputROI, "A", cv::Point(puntoA+10, 20), CV_FONT_HERSHEY_PLAIN
		, 2.0, amarillo, 2, 8);

	//Punto B
	cv::Point P2a = cv::Point(puntoB, 0);
	cv::Point P2b = cv::Point(puntoB, inputROI.rows);
	cv::line(inputROI, P2a, P2b, azul, 1, 0);
	cv::putText(inputROI, "B", cv::Point(puntoB+10, 20), CV_FONT_HERSHEY_PLAIN
		, 2.0, amarillo, 2, 8);



	for (std::map<cvb::CvID, cvb::CvTrack*>::iterator it = tracks.begin(); it != tracks.end(); it++)
	{
		cvb::CvID id = (*it).first;
		cvb::CvTrack* track = (*it).second;
		CvPoint2D64f centroid = track->centroid;
		(*Xposiciones)[id - 1].push_back(centroid.x);

		double areaactual = 0; double L1 = track->maxx - track->minx; double L2 = track->maxy - track->miny;
		areaactual = L1*L2; int PersonasEnBlob=0;
		cv::putText(inputROI, to_string(areaactual), cv::Point(centroid.x,centroid.y), CV_FONT_HERSHEY_PLAIN
		, 2.0, amarillo, 2, 8);
		cv::putText(inputROI, to_string(PersonasEnBlob), cv::Point(centroid.x,centroid.y), CV_FONT_HERSHEY_PLAIN
		, 1.0, rojo, 2, 8);
		if (areaactual > minArea && areaactual < maxArea)
			PersonasEnBlob = 1;
		if (areaactual > maxArea && areaactual < 2 * maxArea)
			PersonasEnBlob = 2;
		if (areaactual > 2 * maxArea && areaactual < 3 * maxArea)
			PersonasEnBlob = 3;
		if (areaactual > 4 * maxArea && areaactual < 5 * maxArea)
			PersonasEnBlob = 4;
			
		cv::putText(inputROI, to_string(PersonasEnBlob), cv::Point(centroid.x+10,centroid.y+10), CV_FONT_HERSHEY_PLAIN
		, 1.0, rojo, 2, 8);

		// Se verifica si paso A->, A<-, B-> o B<- y se almacenan el conjunto de pasos
		// en el vector trayectora [A->,B->,A<-,B<-]  para los modos 1 y 2
		// 1 - Pas� por el punto en esa dir
		// 0 - No pas�

		// conteo en modo 1
		if (modo == 1) {
			if ((*Xposiciones)[id - 1].size() > 1){

				//Punto A punto presente; Punto B punto pasado
				double puntoA = (*Xposiciones)[id - 1][(*Xposiciones)[id - 1].size() - 1];
				double puntoB = (*Xposiciones)[id - 1][(*Xposiciones)[id - 1].size() - 2];
				double distAB = abs(puntoA - puntoB); double largo = inputROI.cols;
				double ratio = distAB / largo;
				//A-> 
				if (puntoA > P1a.x && puntoB < P1a.x && ratio < 0.3){
					(*trayectorias)[id - 1][0] = 1;
				}
				//B->
				if (puntoA > P2a.x && puntoB < P2a.x && ratio < 0.3){
					(*trayectorias)[id - 1][1] = 1;
				}
				//A<-
				if (puntoA < P1a.x && puntoB > P1a.x && ratio < 0.3){
					(*trayectorias)[id - 1][2] = 1;
				}
				//B<-
				if (puntoA < P2a.x && puntoB > P2a.x && ratio < 0.3){
					(*trayectorias)[id - 1][3] = 1;
				}

				//Cuentas In/Out IN:A->B Out B->A

				if ((*trayectorias)[id - 1] == cv::Vec4i(0, 0, 1, 1)){
					*salientes=*salientes+PersonasEnBlob;
					(*trayectorias)[id - 1] = cv::Vec4i(0, 0, 0, 0);
					(*Xposiciones)[id - 1].clear();
				}

				if ((*trayectorias)[id - 1] == cv::Vec4i(1, 1, 0, 0)){
					*entrantes = *entrantes + PersonasEnBlob;
					(*trayectorias)[id - 1] = cv::Vec4i(0, 0, 0, 0);
					(*Xposiciones)[id - 1].clear();
				}
			}
		}
		//conteo en modo 2
		if (modo == 2) {
			if ((*Xposiciones)[id - 1].size() > 1){

				//Punto A punto presente; Punto B punto pasado
				double puntoA = (*Xposiciones)[id - 1][(*Xposiciones)[id - 1].size() - 1];
				double puntoB = (*Xposiciones)[id - 1][(*Xposiciones)[id - 1].size() - 2];
				double distAB = abs(puntoA - puntoB); double largo = inputROI.cols;
				double ratio = distAB / largo;
				//B->
				if (puntoA > P2a.x && puntoB < P2a.x && ratio < 0.3){
					(*trayectorias)[id - 1][1] = 1;

				}
				//A<-
				if (puntoA < P1a.x && puntoB > P1a.x && ratio < 0.3){
					(*trayectorias)[id - 1][2] = 1;
				}

				//Cuentas In/Out IN:A->B Out B->A

				if ((*trayectorias)[id - 1] == cv::Vec4i(0, 0, 1, 0)){
					*salientes = *salientes + PersonasEnBlob;
					(*trayectorias)[id - 1] = cv::Vec4i(0, 0, 0, 0);
					(*Xposiciones)[id - 1].clear();
				}

				if ((*trayectorias)[id - 1] == cv::Vec4i(0, 1, 0, 0)){
					*entrantes = *entrantes + PersonasEnBlob;
					(*trayectorias)[id - 1] = cv::Vec4i(0, 0, 0, 0);
					(*Xposiciones)[id - 1].clear();
				}
			}
		}

	}
	//Se muestra la cuenta
	const string in = "IN= " + to_string(*entrantes);
	cv::putText(inputROI, in, cv::Point(10, 10), CV_FONT_HERSHEY_PLAIN
		, 1.0, rojo, 1, 8);

	const string salida = "OUT= " + to_string(*salientes);
	cv::putText(inputROI, salida, cv::Point(10, 30), CV_FONT_HERSHEY_PLAIN
		, 1.0, verde, 1, 8);

}
