// ContadorInOut.cpp: define el punto de entrada de la aplicación de consola.
//

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <numeric>
#include <opencv2/opencv.hpp>
#include "cvblob.h"
#include "Preprocesador.cpp"
#include "BlobDetector.cpp"
#include "Contador.cpp"
using namespace std;
using namespace cv;
int posx=10;int posy=10;
void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
     if  ( event == EVENT_LBUTTONDOWN )
     {
		posx=x;posy=y;
     }
}

int main()
{
	//string path = "C:\\Users\\Wladimir\\Videos\\personasEscaleras.avi";
	string path = "data/personas.avi";
	cv::VideoCapture cap(path);
	cv::Ptr<cv::BackgroundSubtractor> pMOG; //MOG Background subtractor
	pMOG = new cv::BackgroundSubtractorMOG(); //MOG approach
	cv::Mat frame;
	cvb::CvTracks tracks;
	cv::vector<cv::vector<double> > Xposiciones;
	cv::vector<cv::Vec4i> trayectorias;
	Xposiciones.resize(100);
	trayectorias.resize(100);
	int blur=5;
	int erosion=13;

	

	int IN=0; 
	int OUT=0;
	Preprocesador preprocesador;
	BlobDetector detector;
	Contador contador;

	while (1){

		bool bSuccess = cap.read(frame); // read a new frame from video
		if (!bSuccess){
			cout << "Cannot read a frame from video stream" << endl;
			break;
		}
		
		cv::Mat img_input = frame.clone();
		cv::Rect ROI = cv::Rect(175, 71,237,251);
		cv::namedWindow("origina", 1);
		cv::createTrackbar("Blur Filter", "origina", &blur, 11, NULL );
		cv::createTrackbar("Erosion", "origina", &erosion, 13, NULL );
		preprocesador.setInput(img_input,blur+1,erosion+1, ROI, pMOG);
		cv::Mat inputROI = preprocesador.getImgROI();
		cv::Mat outPreprocessing = preprocesador.start();
		cv::imshow("fpndo", outPreprocessing);
		cv::setMouseCallback("origina", CallBackFunc, NULL);
		cv::putText(frame, to_string(posx)+" , "+to_string(posy), cv::Point(posx,posy), cv::FONT_HERSHEY_SIMPLEX, 0.7, cv::Scalar(200,200,250), 2, 8);
		detector.setparameters(inputROI, outPreprocessing, 4500, 8000, 5, 20);
		detector.getTracks(&tracks);
		cv::Mat locations;   // output, locations of non-zero pixels
		cv::findNonZero(outPreprocessing, locations);
		float zeroPix = (float)locations.rows / (float)(outPreprocessing.rows*outPreprocessing.cols);
		//Conteo y registro de las pistas
		contador.setParametros(&inputROI, tracks, 170 - 0, 120 - 0, 1 , 4500, 8000  );
		contador.iniciarConteo(&Xposiciones, &trayectorias, &IN, &OUT);
		cv::imshow("senales", inputROI);
		cv::imshow("origina", frame);
		if (cv::waitKey(10) == 27) {

			break;
		}

	}



	return 0;
}


