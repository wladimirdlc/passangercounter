//
//  Hello World client in C++
//  Connects REQ socket to tcp://localhost:5555
//  Sends "Hello" to server, expects "World" back
//
#include <zmq.hpp>
#include <string>
#include <iostream>

int main ()
{
    //  Prepare our context and socket
    zmq::context_t context (1);
    zmq::socket_t socket (context, ZMQ_REQ);

    std::cout << "Conectando al servidor de gps…" << std::endl;
    socket.connect ("tcp://localhost:5556");
    for (;;) {
        zmq::message_t request (5);
        memcpy (request.data (), "Hello", 5);
        std::cout << "Sending Hello " << "…" << std::endl;
        socket.send (request);

        //  Get the reply.
        zmq::message_t reply;
        socket.recv (&reply);
		std::string rpl = std::string(static_cast<char*>(reply.data()), reply.size());
        std::cout << "Datos Recibidos: " << rpl << std::endl;
    }
    return 0;
}
