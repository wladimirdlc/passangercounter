#pragma once
#include <opencv2/opencv.hpp>

class Preprocesador
{

private:
	cv::Mat input;//imagen de entrada
	int blursize; //tamano del kernel para el efecto blur
	int erosionsize; //tamano del elemento para la dilation
	cv::Rect ROI; // region de interes
	cv::Ptr<cv::BackgroundSubtractor> pMOG; //puntero del susbstractor de fondo



public:
	Preprocesador();
	~Preprocesador();
	cv::Mat start();
	void setInput(cv::Mat _in, int _blur, int _erosion, cv::Rect _roi, 
		cv::Ptr<cv::BackgroundSubtractor> _pMOG);
	cv::Mat getImgROI();

};

