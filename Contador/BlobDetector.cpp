
#include "BlobDetector.h"


BlobDetector::BlobDetector()
{
}


BlobDetector::~BlobDetector()
{
}

void BlobDetector::setparameters(cv::Mat _input, cv::Mat _procInput, int _minarea, int _maxarea, int _thdistance
	, int _thinactive) {

	inputROI = _input;
	processedInput = _procInput;
	minarea = _minarea;
	maxarea = _maxarea;
	thdistance = _thdistance;
	thinactive = _thinactive;

}

void BlobDetector::getTracks(cvb::CvTracks *tracks) {
	IplImage* inputROIimpl = new IplImage(inputROI);
	IplImage* frameIN = new IplImage(processedInput);
	IplImage *frameLbl = cvCreateImage(cvGetSize(frameIN), IPL_DEPTH_LABEL, 1);
	cvb::CvBlobs blobs;
	unsigned int result = cvb::cvLabel(frameIN, frameLbl, blobs);
	cvb::cvFilterByArea(blobs, minarea, maxarea);
	IplImage *imgOut = cvCreateImage(cvGetSize(frameLbl), IPL_DEPTH_8U, 3); cvZero(imgOut);
	cvRenderBlobs(frameLbl, blobs, frameIN, imgOut);
	cv::Mat out(imgOut);
	cvUpdateTracks(blobs, *tracks, 3., 10);
	cvRenderTracks(*tracks, inputROIimpl, inputROIimpl, 
		CV_TRACK_RENDER_ID | CV_TRACK_RENDER_BOUNDING_BOX | CV_TRACK_RENDER_TO_LOG);
	delete inputROIimpl;
	delete frameIN;
	cvReleaseImage(&frameLbl);
	cvReleaseImage(&imgOut);
	cvb::cvReleaseBlobs(blobs); 

}