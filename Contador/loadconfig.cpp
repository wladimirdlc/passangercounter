
#include "loadconfig.h"
#include <opencv2/opencv.hpp>

using namespace std;

loadconfig::loadconfig()
{
}


loadconfig::~loadconfig()
{
}
void loadconfig::setparameters(int *xo,int *y0,int *ancho, int *alto,
	int *blur, int *erosion, int *puntoa,int *puntob,
	int *minarea, int *maxarea, int *thdis, int *thinact,
	int *velocidadthr, int *modo, int *xres, int *yres){
	cv::FileStorage xmlR;
	xmlR.open("data/config.xml",cv::FileStorage::READ);
	*xo=(int) xmlR["X"];
	*y0=(int) xmlR["Y"];
	*ancho=(int) xmlR["ANCHOROI"];
	*alto=(int) xmlR["ALTOROI"];
	*blur=(int) xmlR["BLUR"];
	*erosion=(int) xmlR["EROSION"];
	*puntoa=(int) xmlR["PUNTOA"];
	*puntob=(int) xmlR["PUNTOB"];
	*minarea=(int) xmlR["MINAREA"];
	*maxarea=(int) xmlR["MAXAREA"];
	*thdis=(int) xmlR["THDISTANCE"];
	*thinact=(int) xmlR["THINACTIVE"];
	*velocidadthr=(int) xmlR["VELOCIDAD"];
	*modo=(int) xmlR["MODOCONTEO"];
	*xres=(int) xmlR["XRESOL"];
	*yres=(int) xmlR["YRESOL"];
	xmlR.release();
}
