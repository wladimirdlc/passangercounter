
#include "Preprocesador.h"


Preprocesador::Preprocesador()
{
}


Preprocesador::~Preprocesador()
{
}

void Preprocesador::setInput(cv::Mat _in, int _blur, int _erosion, cv::Rect _roi,
	cv::Ptr<cv::BackgroundSubtractor> _pMOG) {
	input = _in;
	blursize = _blur;
	erosionsize = _erosion;
	ROI = _roi;
	pMOG = _pMOG;
}

cv::Mat Preprocesador::start(){
	cv::Mat img_input = input.clone();	
	cv::Mat inputROI = img_input(ROI);
	cv::blur(inputROI, inputROI, cv::Size(blursize, blursize));
	//Extracción del fondo
	cv::Mat fgMaskMOG;
	pMOG->operator()(inputROI, fgMaskMOG);
	int erosion_size = erosionsize;
	cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT,
		cv::Size(2 * erosion_size + 1, 2 * erosion_size + 1),
		cv::Point(erosion_size, erosion_size));
	cv::Mat dstdilate;
	cv::dilate(fgMaskMOG, dstdilate, element);
	cv::threshold(dstdilate, dstdilate, 250, 255, CV_THRESH_BINARY);

	return dstdilate;
}
cv::Mat Preprocesador::getImgROI() {
	return input(ROI);
}