#pragma once
#include <opencv2/opencv.hpp>
#include "cvblob.h"
class Contador
{
private:
	cv::Mat inputROI; //imagen de entrada en la region de interes
	cvb::CvTracks tracks;
	int puntoA; //puntos para establecer las lineas de conteo
	int puntoB;
	int modo; //modo de conteo
	int minArea;
	int maxArea;

public:
	Contador();
	~Contador();
	int entrantesAB=0; //A->B
	int salientesBA=0; //B->A
	void setParametros(cv::Mat *_input, cvb::CvTracks _tracks, int _puntoA, int _puntoB, 
		int _modo, int _minarea, int _maxarea);
	void iniciarConteo(cv::vector < cv::vector<double> > *Xposiciones,cv::vector<cv::Vec4i> *trayectorias,
		int *entrantes, int *salientes);

};

