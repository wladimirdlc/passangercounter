// ContadorInOut.cpp: define el punto de entrada de la aplicación de consola.
//

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <numeric>
#include <opencv2/opencv.hpp>
#include "cvblob.h"
#include "Preprocesador.cpp"
#include "BlobDetector.cpp"
#include "Contador.cpp"
#include "loadconfig.cpp"
#include <fstream>
#include <sstream>
#include <zmq.hpp>
#include <sstream>
#include <time.h> 
#include <ctime>
#include <wiringPi.h>

using namespace std;

int main()
{
	//string path = "C:\\Users\\Wladimir\\Videos\\personasEscaleras.avi";
	string path = "data/personas.avi";
	cv::VideoCapture cap(0);
	cv::Ptr<cv::BackgroundSubtractor> pMOG; //MOG Background subtractor
	pMOG = new cv::BackgroundSubtractorMOG(); //MOG approach
	cv::Mat frame;
	cvb::CvTracks tracks;
	cv::vector<cv::vector<double> > Xposiciones;
	cv::vector<cv::Vec4i> trayectorias;
	Xposiciones.resize(100);
	trayectorias.resize(100);
	//leer parametros desde XML
	loadconfig cargarDatos;
	int xo=0;int y0=0;int ancho=100; int alto=100;
	int blur=5; int erosion =11; int puntoa=50;int puntob=50;
	int minarea=2000; int maxarea=5000; int thdis=5; int thinact=20;
	int velocidadthr=10; int modo=1; int xres=640; int yres=480;
	cargarDatos.setparameters( &xo, &y0, &ancho,  &alto,
	 &blur,  &erosion,  &puntoa, &puntob,
	 &minarea,  &maxarea,  &thdis,  &thinact,
	 &velocidadthr,  &modo,  &xres,  &yres);
	cap.set(CV_CAP_PROP_FRAME_WIDTH,xres);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT,yres);
	int IN=0; 
	int OUT=0;
	bool escribirdatos=false;
	Preprocesador preprocesador;
	BlobDetector detector;
	Contador contador;
	wiringPiSetup () ;
	pinMode (25, OUTPUT) ;
	pinMode (24, OUTPUT);	
	//  Prepare our context and socket
    zmq::context_t context (1);
    zmq::socket_t socket (context, ZMQ_REQ);
    std::cout << "Conectando al servidor de gps…" << std::endl;
    socket.connect ("tcp://localhost:5556");
	cv::Mat imagestby(50, 50, CV_8UC3, cv::Scalar(0,0,0));
	digitalWrite (25, HIGH) ; digitalWrite (24, LOW) ;				

	delay(1000);
	while (1){
		//Request de los datos del gps
		zmq::message_t request (5);
        memcpy (request.data (), "Hello", 5);
        //std::cout << "Sending Hello " << "…" << std::endl;
        socket.send (request);
        //  Get the reply.
        zmq::message_t reply;
        socket.recv (&reply);
		std::string rpl = std::string(static_cast<char*>(reply.data()), reply.size());
        //std::cout << "Datos Recibidos: " << rpl << std::endl;
		std::istringstream ss(rpl);
		cv::vector<string> posYvelocidad;std::string token;
		while(std::getline(ss, token, ',')) {
			posYvelocidad.push_back(token); 
		}	

		float velocidad =::atof(posYvelocidad[2].c_str());
		float latitud =::atof(posYvelocidad[0].c_str());
		float longitud =::atof(posYvelocidad[1].c_str());
		
		cv::imshow("stby",imagestby);
		if (velocidad<velocidadthr){
			escribirdatos=true;
			bool bSuccess = cap.read(frame); // read a new frame from video
			if (!bSuccess){
				cout << "Cannot read a frame from video stream" << endl;
				break;
			}
			cv::imshow("origina", frame);
			cv::Mat img_input = frame.clone();
			cv::Rect ROI = cv::Rect(xo, y0, ancho, alto);
			preprocesador.setInput(img_input,blur,erosion, ROI, pMOG);
			cv::Mat inputROI = preprocesador.getImgROI();
			cv::Mat outPreprocessing = preprocesador.start();
			cv::imshow("fondo", outPreprocessing);
			//Detección y seguimiento de los blobs
			detector.setparameters(inputROI, outPreprocessing, minarea,maxarea, thdis, thinact);
			detector.getTracks(&tracks);
			cv::Mat locations;   // output, locations of non-zero pixels
			cv::findNonZero(outPreprocessing, locations);
			//float zeroPix = (float)locations.rows / (float)(outPreprocessing.rows*outPreprocessing.cols);
			//Conteo y registro de las pistas
			contador.setParametros(&inputROI, tracks, puntoa - xo, puntob - xo, modo , minarea, maxarea  );
			int IN_anterior = IN; int OUT_anterior =OUT;
			contador.iniciarConteo(&Xposiciones, &trayectorias, &IN, &OUT);
			/*if (IN_anterior != IN){
				digitalWrite (24, HIGH) ; delay (10);
				digitalWrite (24, LOW) ; delay (10);
				digitalWrite (24, HIGH) ; delay (10);
				digitalWrite (24, LOW) ;				
			}
			if (IN_anterior != IN){
				digitalWrite (24, HIGH) ; delay (10);
				digitalWrite (24, LOW) ; delay (10);				
			}*/		
			cv::imshow("senales", inputROI);			
		}
		if (velocidad>velocidadthr*1.2 and escribirdatos == true){
			  digitalWrite (25, LOW); 
			  std::ofstream outfile;
			  std::string datos2save; 
			  time_t rawtime;
			  struct tm * timeinfo;
			  char buffer[80];
			  time (&rawtime);
			  timeinfo = localtime(&rawtime);
			  strftime(buffer,80,"%d-%m-%Y %I:%M:%S",timeinfo);
			  std::string dateAjoutSysteme(buffer);
			  datos2save = dateAjoutSysteme + "," + to_string(latitud) + "," + to_string(longitud)
				+ "," + to_string(IN) + "," + to_string(OUT) + "\n";			  
			  outfile.open("salida.dlc", std::ios_base::app);
			  outfile << datos2save; 
			  escribirdatos = false;
			  break;
		}
		char le = cv::waitKey(10);
			if (le == 'k'  || le == 'K') {
				break;
			}

	}


	digitalWrite (25, LOW) ;
	return 0;
}

