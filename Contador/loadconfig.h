#pragma once
#include <opencv2/opencv.hpp>
#include "cvblob.h"
class loadconfig
{
private:

public:
	loadconfig();
	~loadconfig();
	
	void setparameters(int *xo,int *y0,int *ancho, int *alto,
	int *blur, int *erosion, int *puntoa,int *puntob,
	int *minarea, int *maxarea, int *thdis, int *thinact,
	int *velocidadthr, int *modo, int *xres, int *yres);

};

