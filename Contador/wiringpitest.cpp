#include <wiringPi.h>
int main (void)
{
  wiringPiSetup () ;
  pinMode (24, OUTPUT) ;
  for (;;)
  {
    digitalWrite (24, HIGH) ; delay (500) ;
    digitalWrite (24,  LOW) ; delay (500) ;
  }
  return 0 ;
}
