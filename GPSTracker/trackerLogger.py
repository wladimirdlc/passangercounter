
# Importamos los modulos necesarios
import os
from gps import *
from time import *
import time
import datetime
import threading
import sys
import math
import numpy as np
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(21, GPIO.OUT)#Led Rojo es salida

gpsd = None #Declaramos la variable GPSD
#Este paso es opcional ya que no hace falta que muestre ningun mensaje en la terminal
#Si ponemos el script como un 'daemon'
os.system('clear') #Limpiamos la terminal 
#Este paso es opcional ya que no hace falta que muestre ningun mensaje en la terminal
#Si ponemos el script como un 'daemon'
 
class GpsPoller(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        global gpsd #Declaramos GPSD como una variable global
        gpsd = gps(mode=WATCH_ENABLE) #Iniciamos el streaming de datos GPS
        self.current_value = None
        self.running = True 
 
    def run(self):
        global gpsd
        while gpsp.running:
            gpsd.next() #Esto continuara el loop y recojera todos los datos para limpiar el buffer
 
 
 
if __name__ == '__main__':
    gpsp = GpsPoller() # Creamos el thread para recibir datos del modulo GPS
    port = "5556"
    GPIO.output(21,False) 
    gpsp.start() # Y lo arrancamos
    firstTime = True
    while True: #Iniciamos un bucle
		os.system('clear')
		print "GPS Logger..."	
		print 'Latitud: ' , gpsd.fix.latitude #Sacamos por pantalla la latitud
		print 'Longitud: ' , gpsd.fix.longitude #Sacamos por pantalla la longitud
		print 'Velocidad: ', 3.6*gpsd.fix.speed
		print 'Fecha y Hora: ', datetime.datetime.now()
		#Wait for next request from client
		if ((gpsd.fix.latitude == 0.0 and gpsd.fix.longitude == 0.0) or math.isnan(gpsd.fix.latitude) or math.isnan(gpsd.fix.longitude)):
			#Sacamos por pantalla  este mensaje
			print "Esperando GPS..."
			espera = 0.5
			for i in xrange(1,5):
				GPIO.output(21,True)
				time.sleep(0.5)      
				GPIO.output(21,False) 
				time.sleep(0.5) 
		else:
			espera=15
			#En caso contrario sacamos etse mensaje
			print "GPS OK"
			for i in xrange (1,10):
				GPIO.output(21,False)
				time.sleep(0.2)      
				GPIO.output(21,True) 
				time.sleep(0.2) 						
			datos_disponibles = np.array([gpsd.fix.latitude, gpsd.fix.longitude, 3.6*gpsd.fix.speed])
			#Guardamos los datos en un archivo
			#(uso 'a' para agregar el texto. si usase 'w' solo se guardarian las ultimas coordenadas)
			data = open("locationsALL.txt", "a")
			data.write("%s,%s,%s,%s\n" % (datetime.datetime.now(),gpsd.fix.latitude, gpsd.fix.longitude, 3.6*gpsd.fix.speed))
			data.close()	
			
		time.sleep(espera)
	
			
