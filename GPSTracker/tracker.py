
# Importamos los modulos necesarios
import os
from gps import *
from time import *
import time
import datetime
import threading
import zmq
import sys
import math
import numpy as np
gpsd = None #Declaramos la variable GPSD
#Este paso es opcional ya que no hace falta que muestre ningun mensaje en la terminal
#Si ponemos el script como un 'daemon'
os.system('clear') #Limpiamos la terminal 
#Este paso es opcional ya que no hace falta que muestre ningun mensaje en la terminal
#Si ponemos el script como un 'daemon'
 
class GpsPoller(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        global gpsd #Declaramos GPSD como una variable global
        gpsd = gps(mode=WATCH_ENABLE) #Iniciamos el streaming de datos GPS
        self.current_value = None
        self.running = True 
 
    def run(self):
        global gpsd
        while gpsp.running:
            gpsd.next() #Esto continuara el loop y recojera todos los datos para limpiar el buffer
 
 
 
if __name__ == '__main__':
    gpsp = GpsPoller() # Creamos el thread para recibir datos del modulo GPS
    port = "5556"
    if len(sys.argv) > 1:
		port =  sys.argv[1]
		int(port)
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://*:%s" % port)
    gpsp.start() # Y lo arrancamos
    firstTime = True
    k=0
    while True: #Iniciamos un bucle
		if (firstTime == True):
			datos_disponibles=np.array([0,0,1])
			firstTime=False
		os.system('clear')
		print 'Latitud: ' , gpsd.fix.latitude #Sacamos por pantalla la latitud
		print 'Longitud: ' , gpsd.fix.longitude #Sacamos por pantalla la longitud
		print 'Velocidad: ', 3.6*gpsd.fix.speed
		print 'Fecha y Hora: ', datetime.datetime.now()
		print datos_disponibles
		#Wait for next request from client
		message = socket.recv()
		print "Received request: ", message		
		if ((gpsd.fix.latitude == 0.0 and gpsd.fix.longitude == 0.0) or math.isnan(gpsd.fix.latitude) or math.isnan(gpsd.fix.longitude)):
			#Sacamos por pantalla  este mensaje
			print "Esperando GPS..."
		else:
			#En caso contrario sacamos etse mensaje
			print "GPS OK"			
			datos_disponibles = np.array([gpsd.fix.latitude, gpsd.fix.longitude, 3.6*gpsd.fix.speed])
			firstTime = False
		str2send=str(datos_disponibles[0])+","+str(datos_disponibles[1])+","+str(datos_disponibles[2])+" %s"	
		print 'enviando datos disponibles: ',str2send
		socket.send(str2send % port)
	
			
