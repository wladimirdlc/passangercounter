import os
import time
import datetime
import sys
import subprocess
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)
input_state = GPIO.input(23)
if input_state == False:
	print('Button Pressed')
	estadoCOPY=True
	time.sleep(0.2)
if input_state == True:
	print('Button NOT Pressed')
	estadoCOPY=False
	time.sleep(0.2)
if (estadoCOPY==False):
	p=subprocess.Popen(['lxterminal -e sudo python /home/pi/projects/GlobalExcecution/StartStopContbusdlc.py'],shell=True,stdin=None, stdout=None, stderr=None, close_fds=True)
	print "abierto COntador"
	p=subprocess.Popen(["lxterminal -e sudo python /home/pi/projects/GPSTracker/tracker.py"],shell=True,stdin=None, stdout=None, stderr=None, close_fds=True)
	print "abierto GPS"
	p=subprocess.Popen(["lxterminal -e sudo python /home/pi/projects/GPSTracker/tracker.py"],shell=True,stdin=None, stdout=None, stderr=None, close_fds=True)
	print "abierto GPS Logger"
	p=subprocess.Popen(["lxterminal -e sudo python /home/pi/projects/GPSTracker/trackerLogger.py"],shell=True,stdin=None, stdout=None, stderr=None, close_fds=True)
if (estadoCOPY==True):
	print "Modo Copiar"
	p=subprocess.Popen(["lxterminal -e sudo python /home/pi/projects/CopyData/copydata.py"],shell=True,stdin=None, stdout=None, stderr=None, close_fds=True)


