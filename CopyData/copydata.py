import os
from glob import glob
from subprocess import check_output, CalledProcessError
import numpy as np
import shutil
import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setup(21, GPIO.OUT)#Led Rojo es salida


def get_usb_devices():
    sdb_devices = map(os.path.realpath, glob('/sys/block/sd*'))
    usb_devices = (dev for dev in sdb_devices
        if 'usb' in dev.split('/')[5])
    return dict((os.path.basename(dev), dev) for dev in usb_devices)

def get_mount_points(devices=None):
    devices = devices or get_usb_devices() # if devices are None: get_usb_devices
    output = check_output(['mount']).splitlines()
    is_usb = lambda path: any(dev in path for dev in devices)
    usb_info = (line for line in output if is_usb(line.split()[0]))
    return [(info.split()[0], info.split()[2]) for info in usb_info]

if __name__ == '__main__':
	raiz = np.array(get_mount_points())
	dstroot=raiz[0][1]
	srcfile='/home/pi/projects/Contador/salida.dlc'
	shutil.copy2(srcfile,dstroot)
	for i in xrange (1,20):
		GPIO.output(21,False)
		time.sleep(0.05)      
		GPIO.output(21,True) 
		time.sleep(0.05)
		GPIO.output(21,False)
 
		
	
